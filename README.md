# Task List

## ¿Qué es?

Web que permite definir **tareas** por hacer, con su titulo y descripción.
Las tareas se pueden completar y si están completadas se pueden eliminar.

En definitiva es un **CRUD** *(Create Read Update & Delete)* web.

Hecho mediante Bootstrap y JavaScript.

Los datos se almacenan en el `LocalStorage` del navegador web.


## Servidor de Desarrollo

Se ejecuta en un servidor web básico mediante 
`serve` *(instalar con `npm i serve`)*.

```shell
disce@host ~/work/tareas $ npx serve -l 8080 

   ┌───────────────────────────────────────────────────┐
   │                                                   │
   │   Serving!                                        │
   │                                                   │
   │   - Local:            http://localhost:8080       │
   │   - On Your Network:  http://xxx.xxx.xxx.xxx:8080 │
   │                                                   │
   │   Copied local address to clipboard!              │
   │                                                   │
   └───────────────────────────────────────────────────┘

```
