export {readLocalStorage, writeLocalStorage}
import Task from './task.js'

const localStorage = window.localStorage // Get localStorage for browser

/* Write data in localStorage browser */
function writeLocalStorage(taskArray) {
    localStorage.tasks = JSON.stringify(taskArray)
}

function readLocalStorage(taskArray) {
    if (localStorage.length) {
        let localArray = JSON.parse(localStorage.getItem("tasks"))
        pushData(taskArray, localArray)
    }
}

/* Save data from localStorage */
function pushData(taskArray, localArray) {
    var size = localArray.length

    for (let i = 0; i < size; i++) {
        let task = new Task(
            localArray[i].id,
            localArray[i].title,
            localArray[i].desc,
            localArray[i].completed)

        taskArray.push(task)
        task = null
    }
}