import * as front from './front.js'

document.addEventListener("DOMContentLoaded", function() {

    const add_btn = document.getElementById("add_btn")    

    front.existsTasks()
    add_btn.addEventListener("click", front.addTask)

})